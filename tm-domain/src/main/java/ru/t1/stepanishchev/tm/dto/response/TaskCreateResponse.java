package ru.t1.stepanishchev.tm.dto.response;

import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.model.Task;

public final class TaskCreateResponse extends AbstractTaskResponse {

    public TaskCreateResponse(@Nullable final Task task) {
        super(task);
    }

}