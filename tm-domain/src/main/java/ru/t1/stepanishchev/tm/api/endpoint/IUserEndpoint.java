package ru.t1.stepanishchev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.stepanishchev.tm.dto.request.*;
import ru.t1.stepanishchev.tm.dto.response.*;

public interface IUserEndpoint {

    @NotNull
    UserLockResponse lockUser(@NotNull UserLockRequest request);

    @NotNull
    UserUnlockResponse unlockUser(@NotNull UserUnlockRequest request);

    @NotNull
    UserRegistryResponse registryUser(@NotNull UserRegistryRequest request);

    @NotNull
    UserRemoveResponse removeUser(@NotNull UserRemoveRequest request);

    @NotNull
    UserUpdateProfileResponse updateProfileUser(@NotNull UserUpdateProfileRequest request);

    @NotNull
    UserProfileResponse viewProfileUser(@NotNull UserProfileRequest request);

    @NotNull
    UserChangePasswordResponse changePassword(@NotNull UserChangePasswordRequest request);

}