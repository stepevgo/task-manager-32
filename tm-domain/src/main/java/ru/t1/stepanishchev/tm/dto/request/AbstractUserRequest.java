package ru.t1.stepanishchev.tm.dto.request;

import lombok.Setter;
import lombok.Getter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public abstract class AbstractUserRequest extends AbstractRequest {

    @Nullable
    private String userId;

}