package ru.t1.stepanishchev.tm.dto.request;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectChangeStatusByIndexRequest extends AbstractIndexRequest {

    @Nullable
    private Status status;

    public ProjectChangeStatusByIndexRequest(@Nullable final Integer index, @Nullable final Status status) {
        super(index);
        this.status = status;
    }

}