package ru.t1.stepanishchev.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class TaskShowByIdRequest extends AbstractIdRequest {

    public TaskShowByIdRequest(@Nullable final String id) {
        super(id);
    }

}