package ru.t1.stepanishchev.tm.dto.response;

import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.model.User;

public final class UserRemoveResponse extends AbstractUserResponse {

    public UserRemoveResponse(@Nullable final User user) {
        super(user);
    }

}