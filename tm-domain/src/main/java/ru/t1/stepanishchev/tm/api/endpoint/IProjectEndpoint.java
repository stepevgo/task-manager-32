package ru.t1.stepanishchev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.stepanishchev.tm.dto.request.*;
import ru.t1.stepanishchev.tm.dto.response.*;

public interface IProjectEndpoint {

    @NotNull
    public ProjectChangeStatusByIdResponse changeProjectStatusById(@NotNull ProjectChangeStatusByIdRequest request);

    @NotNull
    public ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(@NotNull ProjectChangeStatusByIndexRequest request);

    @NotNull
    public ProjectCreateResponse createProject(@NotNull ProjectCreateRequest request);

    @NotNull
    public ProjectListResponse listProject(@NotNull ProjectListRequest request);

    @NotNull
    public ProjectShowByIdResponse showProjectById(@NotNull ProjectShowByIdRequest request);

    @NotNull
    public ProjectShowByIndexResponse showProjectByIndex(@NotNull ProjectShowByIndexRequest request);

    @NotNull
    public ProjectUpdateByIdResponse updateProjectById(@NotNull ProjectUpdateByIdRequest request);

    @NotNull
    public ProjectUpdateByIndexResponse updateProjectByIndex(@NotNull ProjectUpdateByIndexRequest request);

    @NotNull
    public ProjectClearResponse clearProject(@NotNull ProjectClearRequest request);

    @NotNull
    public ProjectRemoveByIdResponse removeProjectById(@NotNull ProjectRemoveByIdRequest request);

    @NotNull
    public ProjectRemoveByIndexResponse removeProjectByIndex(@NotNull ProjectRemoveByIndexRequest request);

    @NotNull
    public ProjectStartByIdResponse startProjectById(@NotNull ProjectStartByIdRequest request);

    @NotNull
    public ProjectStartByIndexResponse startProjectByIndex(@NotNull ProjectStartByIndexRequest request);

    @NotNull
    public ProjectCompleteByIdResponse completeProjectById(@NotNull ProjectCompleteByIdRequest request);

    @NotNull
    public ProjectCompleteByIndexResponse completeProjectByIndex(@NotNull ProjectCompleteByIndexRequest request);

}