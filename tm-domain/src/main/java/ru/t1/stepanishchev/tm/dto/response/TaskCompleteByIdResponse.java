package ru.t1.stepanishchev.tm.dto.response;

import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.model.Task;

public final class TaskCompleteByIdResponse extends AbstractTaskResponse {

    public TaskCompleteByIdResponse(@Nullable final Task task) {
        super(task);
    }

}