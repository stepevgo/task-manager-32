package ru.t1.stepanishchev.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectStartByIdRequest extends AbstractIdRequest {

    public ProjectStartByIdRequest(@Nullable final String id) {
        super(id);
    }

}