package ru.t1.stepanishchev.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public final class TaskClearRequest extends AbstractUserRequest {
}