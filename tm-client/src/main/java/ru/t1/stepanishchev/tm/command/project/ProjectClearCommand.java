package ru.t1.stepanishchev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.stepanishchev.tm.dto.request.ProjectClearRequest;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    private final String NAME = "project-clear";

    @NotNull
    private final String DESCRIPTION = "Remove all projects.";

    @Override
    public void execute() {
        System.out.println("[PROJECTS CLEAR]");
        getProjectEndpoint().clearProject(new ProjectClearRequest());
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}