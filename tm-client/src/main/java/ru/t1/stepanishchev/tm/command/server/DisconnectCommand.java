package ru.t1.stepanishchev.tm.command.server;


import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.command.AbstractCommand;
import ru.t1.stepanishchev.tm.enumerated.Role;

public final class DisconnectCommand extends AbstractCommand {

    @NotNull
    public static final String NAME = "disconnect";

    @Override
    @SneakyThrows
    public void execute() {
        getServiceLocator().getAuthEndpoint().disconnect();
    }

    @Override
    public @Nullable String getName() {
        return null;
    }

    @Override
    public @Nullable Role[] getRoles() {
        return Role.values();
    }

    @Override
    public @Nullable String getDescription() {
        return "Disconnect from server.";
    }

}