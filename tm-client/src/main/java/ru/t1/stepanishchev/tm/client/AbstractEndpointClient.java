package ru.t1.stepanishchev.tm.client;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.stepanishchev.tm.api.endpoint.IEndpointClient;
import ru.t1.stepanishchev.tm.dto.response.ApplicationErrorResponse;

import java.io.*;
import java.net.Socket;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractEndpointClient implements IEndpointClient {

    @NotNull
    private String host = "localhost";

    @NotNull
    private Integer port = 6060;

    @NotNull
    private Socket socket;

    public AbstractEndpointClient(@NotNull final AbstractEndpointClient client) {
        host = client.host;
        port = client.port;
        socket = client.socket;
    }

    @NotNull
    @SneakyThrows
    protected <T> T call(@NotNull final Object data, @NotNull final Class<T> clazz) {
        getObjectOutputStream().writeObject(data);
        @NotNull final Object result = getObjectInputStream().readObject();
        if (result instanceof ApplicationErrorResponse) {
            @NotNull final ApplicationErrorResponse response = (ApplicationErrorResponse) result;
            throw new RuntimeException(response.getMessage());
        }
        return (T) result;
    }

    @NotNull
    private ObjectOutputStream getObjectOutputStream() throws IOException {
        return new ObjectOutputStream(getOutputStream());
    }

    @NotNull
    private ObjectInputStream getObjectInputStream() throws IOException {
        return new ObjectInputStream(getInputStream());
    }

    @NotNull
    @SneakyThrows
    private OutputStream getOutputStream() {
        return socket.getOutputStream();
    }

    @NotNull
    @SneakyThrows
    private InputStream getInputStream() {
        return socket.getInputStream();
    }

    @SneakyThrows
    public Socket connect() {
        socket = new Socket(host, port);
        return socket;
    }

    @SneakyThrows
    public Socket disconnect() {
        socket.close();
        return socket;
    }

}