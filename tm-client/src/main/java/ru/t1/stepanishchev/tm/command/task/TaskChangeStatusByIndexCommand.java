package ru.t1.stepanishchev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.dto.request.TaskChangeStatusByIndexRequest;
import ru.t1.stepanishchev.tm.enumerated.Status;
import ru.t1.stepanishchev.tm.util.TerminalUtil;

import java.util.Arrays;

public final class TaskChangeStatusByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private final String NAME = "task-change-status-by-index";

    @NotNull
    private final String DESCRIPTION = "Change task status by index.";

    @Override
    public void execute() {
        System.out.println("[CHANGE TASK STATUS BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @Nullable final Status status = Status.toStatus(statusValue);
        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest(index, status);
        getTaskEndpoint().changeTaskStatusByIndex(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}