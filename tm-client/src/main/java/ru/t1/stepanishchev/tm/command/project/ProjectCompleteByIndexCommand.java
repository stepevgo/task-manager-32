package ru.t1.stepanishchev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.stepanishchev.tm.dto.request.ProjectChangeStatusByIndexRequest;
import ru.t1.stepanishchev.tm.enumerated.Status;
import ru.t1.stepanishchev.tm.util.TerminalUtil;

public final class ProjectCompleteByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private final String NAME = "project-completed-by-index";

    @NotNull
    private final String DESCRIPTION = "Complete project by index.";

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final ProjectChangeStatusByIndexRequest request =
                new ProjectChangeStatusByIndexRequest(index, Status.COMPLETED);
        getProjectEndpoint().changeProjectStatusByIndex(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}