package ru.t1.stepanishchev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.dto.request.ProjectChangeStatusByIndexRequest;
import ru.t1.stepanishchev.tm.enumerated.Status;
import ru.t1.stepanishchev.tm.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectChangeStatusByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private final String NAME = "project-change-status-by-index";

    @NotNull
    private final String DESCRIPTION = "Change project by index.";

    @Override
    public void execute() {
        System.out.println("[CHANGE PROJECT STATUS BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @Nullable final Status status = Status.toStatus(statusValue);
        @NotNull final ProjectChangeStatusByIndexRequest request =
                new ProjectChangeStatusByIndexRequest(index, status);
        getProjectEndpoint().changeProjectStatusByIndex(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}