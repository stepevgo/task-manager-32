package ru.t1.stepanishchev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.stepanishchev.tm.api.endpoint.IUserEndpointClient;
import ru.t1.stepanishchev.tm.command.AbstractCommand;

public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    protected IUserEndpointClient getUserEndpoint() {
        return getServiceLocator().getUserEndpoint();
    }

}