package ru.t1.stepanishchev.tm.command;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.api.model.ICommand;
import ru.t1.stepanishchev.tm.api.service.IServiceLocator;
import ru.t1.stepanishchev.tm.enumerated.Role;

@Getter
@Setter
public abstract class AbstractCommand implements ICommand {

    @NotNull
    protected IServiceLocator serviceLocator;

    public abstract void execute();

    @Nullable
    public abstract String getName();

    @Nullable
    public String getArgument() {
        return null;
    }

    @Nullable
    public abstract Role[] getRoles();

    @Nullable
    public abstract String getDescription();

    @NotNull
    @Override
    public String toString() {
        @Nullable final String name = getName();
        @Nullable final String argument = getArgument();
        @Nullable final String description = getDescription();
        @NotNull String result = "";
        if (name != null && !name.isEmpty()) result += name + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }

}