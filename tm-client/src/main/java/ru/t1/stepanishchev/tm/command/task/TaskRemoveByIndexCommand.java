package ru.t1.stepanishchev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.stepanishchev.tm.dto.request.TaskRemoveByIndexRequest;
import ru.t1.stepanishchev.tm.util.TerminalUtil;

public final class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private final String NAME = "task-remove-by-index";

    @NotNull
    private final String DESCRIPTION = "Remove task by index.";

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        getTaskEndpoint().removeTaskByIndex(new TaskRemoveByIndexRequest(index));
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}