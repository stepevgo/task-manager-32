package ru.t1.stepanishchev.tm.client;

import org.jetbrains.annotations.NotNull;
import ru.t1.stepanishchev.tm.api.endpoint.ISystemEndpoint;
import ru.t1.stepanishchev.tm.dto.request.ServerAboutRequest;
import ru.t1.stepanishchev.tm.dto.request.ServerVersionRequest;
import ru.t1.stepanishchev.tm.dto.response.ServerAboutResponse;
import ru.t1.stepanishchev.tm.dto.response.ServerVersionResponse;

public final class SystemEndpointClient extends AbstractEndpointClient implements ISystemEndpoint {

    @NotNull
    @Override
    public ServerAboutResponse getAbout(@NotNull ServerAboutRequest request) {
        return call(request, ServerAboutResponse.class);
    }

    @NotNull
    @Override
    public ServerVersionResponse getVersion(@NotNull ServerVersionRequest request) {
        return call(request, ServerVersionResponse.class);
    }

}