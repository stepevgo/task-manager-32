package ru.t1.stepanishchev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.dto.request.ProjectRemoveByIdRequest;
import ru.t1.stepanishchev.tm.dto.response.ProjectRemoveByIdResponse;
import ru.t1.stepanishchev.tm.exception.entity.ProjectNotFoundException;
import ru.t1.stepanishchev.tm.model.Project;
import ru.t1.stepanishchev.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @NotNull
    private final String NAME = "project-remove-by-id";

    @NotNull
    private final String DESCRIPTION = "Remove project by id.";

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(id);
        @NotNull ProjectRemoveByIdResponse response = getProjectEndpoint().removeProjectById(request);
        @Nullable final Project project = response.getProject();
        if (project == null) throw new ProjectNotFoundException();
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}